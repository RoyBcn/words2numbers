**Read a text from the console (path to a file or plain text) and convert text numbers to decimal numbers.**

Developed with Visual Studio 2019 with C++17 standards and CMake ready for windows_64 and Linux systems.

---

## Run

Start the aplication with the executables located in the [/releases](https://bitbucket.org/RoyBcn/words2numbers/src/master/releases/) folder.
To run: *usage words2numbers [text] or [-f <filename>]*

---

## Test
This test is intended to test all the major casuistics:

1. Simple number conversion (forty six).
2. Conversion with dashes (Twenty-three).
3. Conversion witho or without capital letters (Forty).
4. Conversion between special characters ((Forty - three)).
5. Numbers followed by ending symbols like dot or comma(one,).
6. Complex numbers with or without "and"(six million and sixty).

A simple PASS/FAIL test could be done by running: *words2numbers -t*

Sccenario:
"*Twenty-three hundred sixty one, victims were hospitalized (Forty - three) people were injured in the train wreck.The cost ascends to forty six million and sixty six dollars.*"

Expected Result:
"*2361, victims were hospitalized (43) people were injured in the train wreck.The cost ascends to 46000066 dollars.*"


