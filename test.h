using namespace std;

class Test {
public:
  static void printTests();
  
private:
  static void testPrintTextWithDecimalsEnglish();
  static void testPrintFileWithDecimalsEnglish();  
};