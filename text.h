#include <fstream>
#include <string.h>

using namespace std;

class Text {
public:
  static string printTextWithDecimalsEnglish(char*);
  static pair<string, string> printFileWithDecimalsEnglish(ifstream*);

private:
  static void printWordWithDecimalsEnglish(string* word, string* text_number, string* result_sentence);
};