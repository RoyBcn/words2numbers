/*
 * @file test.cc
 *
 * @brief Test class to run a set of tests of textNumbersEnglish class.
 *
 * @ingroup words2numbers
 *
 * @author Roger Comas
 * Contact: rogercomas@hotmail.com
 *
 */

#include "test.h"
#include "text.h"

#include <iostream>

using namespace std;

void Test::testPrintTextWithDecimalsEnglish()
{
  string rest_test = Text::printTextWithDecimalsEnglish("Twenty-three hundred sixty one victims were hospitalized (Forty - three) people were injured in the train wreck.The cost ascends to forty six million and sixty six dollars.");
  
  if (rest_test == "2361 victims were hospitalized (43) people were injured in the train wreck.The cost ascends to 46000066 dollars. ")
    cout << "TEST <printTextWithDecimalsEnglish> PASS." << endl;
  else
    cout << "TEST <printTextWithDecimalsEnglish> FAIL." << endl;
}

void Test::testPrintFileWithDecimalsEnglish()
{
  //TO DO
}

void Test::printTests()
{
  testPrintTextWithDecimalsEnglish();
  testPrintFileWithDecimalsEnglish();
}