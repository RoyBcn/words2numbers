#include <string.h>
#include <unordered_map>

using namespace std;

class TextNumbersEnglish {
public:
  static bool isNumber(string);
  static bool isJoin(string);
  static bool isEnd(string);
  static void truncate(string*);
  static string getBeginingTrim(string);
  static string getEndingTrim(string);
  static string convertToNumbers(string number, string end);

private:
  static int getNumber(string);
  static string toLowerCase(string);
  static string truncateBegining(string);
  static string truncateEnding(string);
  static void removeSpecialCharacters(string*);
  static unordered_map<string, int> numbers_map_;
    
};