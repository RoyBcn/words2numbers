/*
 * @file text.cc
 *
 * @brief Text class to manage input text via ifstream or string and
 * print it in the standard IO replacing all numbers written in text
 * by it's corresponding value in decimals.
 *
 * @ingroup words2numbers
 *
 * @author Roger Comas
 * Contact: rogercomas@hotmail.com
 *
 */

#include "text.h"
#include "textNumbersEnglish.h"

#include <iostream>
#include <sstream>

string Text::printTextWithDecimalsEnglish(char* in_text) {
  string in_text_string(in_text), word, text_number, result_sentence;
  stringstream in_text_stream(in_text_string);

  while (in_text_stream >> word)
    printWordWithDecimalsEnglish(&word, &text_number, &result_sentence);

  //Check if text ends suddenly with a number.
  if (text_number != "")
    result_sentence.append(TextNumbersEnglish::convertToNumbers(text_number, ""));

  return result_sentence;
}

pair<string,string> Text::printFileWithDecimalsEnglish(ifstream* in_file) {
  pair<string, string> result_sentence;
  string word, text_number;  

  while (*in_file >> word)
  {
    result_sentence.first.append(word + " ");
    printWordWithDecimalsEnglish(&word, &text_number, &result_sentence.second);
  }

  //Check if text ends suddenly with a number.
  if (text_number != "")
    result_sentence.second.append(TextNumbersEnglish::convertToNumbers(text_number, ""));

  return result_sentence;
}

void Text::printWordWithDecimalsEnglish(string* word, string* text_number, string* result_sentence) {
  TextNumbersEnglish::truncate(word);
  stringstream str_strm(*word);
  string truncated_word;
  while (str_strm >> truncated_word)
  {
    if (TextNumbersEnglish::isNumber(truncated_word))
    {
      //Differentiate between joins in a number or joins in a sentence.
      if ((*text_number == "") && TextNumbersEnglish::isJoin(truncated_word))
        result_sentence->append(truncated_word + " ");
      else {
        result_sentence->append(TextNumbersEnglish::getBeginingTrim(truncated_word));
        text_number->append(truncated_word + " ");

        if (TextNumbersEnglish::isEnd(truncated_word)) {
          result_sentence->append(TextNumbersEnglish::convertToNumbers(*text_number, truncated_word) + " ");
          *text_number = "";
        }
      }
    }
    else if (*text_number == "")
      result_sentence->append(truncated_word + " ");
    else
    {
      result_sentence->append(TextNumbersEnglish::convertToNumbers(*text_number, truncated_word) + " " + truncated_word + " ");
      *text_number = "";
    }
  }
}