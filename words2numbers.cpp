﻿/*
 * @file words2numbers.cc
 *
 * @brief Read a text from the console (ifstrem file or plain string) and
 * convert text numbers to decimal numbers.
 *
 * @author Roger Comas
 * Contact: rogercomas@hotmail.com
 *
 */

#include "test.h"
#include "text.h"

#include <iostream>
#include <fstream>
#include <string.h>

using namespace std;

void printBeautyConsole(const char* input_text, string result)
{
  cout << "PARSE TEXT TO NUMBERS FROM:" << endl << "> " << input_text << endl;
  cout << "TO:" << endl << "> " << result.c_str() << endl;
}

int main(int argc, char* argv[])
{
  //Test
  if ((argc == 2) && !strcmp(argv[1], "-t"))
    Test::printTests();

  else if (argc == 2)
    printBeautyConsole(argv[1], Text::printTextWithDecimalsEnglish(argv[1]));

  //Convert text file
  else if ((argc == 3) && !strcmp(argv[1], "-f")) {
    ifstream input_file(argv[2]);
    
    //Check file acces
    if (!input_file.is_open())
      cout << "Could not open file\n";
    
    else {
      pair<string, string> result_sentence = Text::printFileWithDecimalsEnglish(&input_file);
      printBeautyConsole(result_sentence.first.c_str(), result_sentence.second);
    }
  }

  //Bad command usage
  else
    cout << "usage: " << argv[0] << " [text] or [-f <filename>]\n";
}
