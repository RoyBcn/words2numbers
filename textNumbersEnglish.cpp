/*
 * @file textNumbersEnglish.cc
 *
 * @brief Class with all the specific methods to parse
 * numbers written in text by it's corresponding value in decimals.
 *
 * @ingroup words2numbers
 *
 * @author Roger Comas
 * Contact: rogercomas@hotmail.com
 *
 */

#include "textNumbersEnglish.h"
#include <iostream>
#include <sstream>
#include <locale>
#include <algorithm>

unordered_map<string,int> TextNumbersEnglish::numbers_map_ = {
  //SPECIFIC NUMBERS
  {"zero", 0},
  {"one", 1},
  {"two", 2},
  {"three", 3},
  {"four", 4},
  {"five", 5},
  {"six", 6},
  {"seven", 7},
  {"eight", 8},
  {"nine", 9},
  {"ten", 10},
  {"eleven", 11},
  {"twelve", 12},
  {"thirteen", 13},
  {"fourteen", 14},
  {"fifteen", 15},
  {"sixteen", 16},
  {"seventeen", 17},
  {"eighteen", 18},
  {"nineteen", 19},
  {"twenty", 20},
  {"thirty", 30},
  {"forty", 40},
  {"fifty", 50},
  {"sixty", 60},
  {"seventy", 70},
  {"eighty", 80},
  {"ninety", 90},

  //MULTYPLIERS
  {"hundred", 100},
  {"thousand", 1000},
  {"million", 1000000},
  {"billion", 1000000000},

  //JOINS
  {"and", 0},
  {"with", 0} };

bool TextNumbersEnglish::isNumber(string word) {
  removeSpecialCharacters(&word);
  string number = "";
  for (auto& x : numbers_map_)
    if (toLowerCase(word).find(x.first) != string::npos)
      number = (x.first.length() > number.length() ? x.first : number);

  return (word.length() == number.length());
}


bool TextNumbersEnglish::isJoin(string join) {
  return (join == "and") || (join == "with");
}

bool TextNumbersEnglish::isEnd(string end) {
  string end_truncated = truncateEnding(end);
  size_t pos = 0;
  for (auto& x : numbers_map_) {
    pos = toLowerCase(end_truncated).find(x.first);
    if ((pos != string::npos) && ((end_truncated.length() == x.first.length()) || (pos == end_truncated.length() - x.first.length())))
      return false;
  }

  return true;
}

void TextNumbersEnglish::truncate(string* word) { 
  for (size_t index = word->find("-", 0); index != std::string::npos; index = word->find("-", index + 1))
    word->replace(index, 1, " ");
}

string TextNumbersEnglish::getBeginingTrim(string begin) {
  //Trim specials characters in the begining of the number, for instance: "(one hundred)"
  string begin_truncated = truncateBegining(begin);
  size_t pos = 0;
  for (auto& x : numbers_map_) {
    pos = toLowerCase(begin_truncated).find(x.first);
    if (pos != string::npos)
      return begin_truncated.substr(0, pos);
  }

  return "";
}

string TextNumbersEnglish::getEndingTrim(string end) {
  //Trim specials characters in the ending of the number, for instance: "one hundred!"size_t pos = 0;
  string end_truncated = truncateEnding(end);
  size_t pos = 0;
  for (auto& x : numbers_map_) {
    pos = toLowerCase(end_truncated).find(x.first);
    if (pos != string::npos)
      return end_truncated.substr(pos + x.first.length(), end_truncated.length());
  }

  return "";
}

string TextNumbersEnglish::convertToNumbers(string number, string end) {
  string word;
  stringstream number_stream(number);
  int decimal_result = 0;
  int decimal_number = 0;

  while ((number_stream >> word)) {
    int tmp_number = getNumber(word);
    if (tmp_number >= 1000)
    {
      decimal_number *= tmp_number;
      decimal_result += decimal_number;
      decimal_number = 0;
    }
    else if (tmp_number >= 100)
    {
      decimal_number *= tmp_number;
    }
    else
      decimal_number += tmp_number;
  }

  return (to_string(decimal_result + decimal_number) + getEndingTrim(truncateEnding(end)));
}

int TextNumbersEnglish::getNumber(string word) {
  pair<string, int> number = { "", 0 };
  for (auto& x : numbers_map_)
    if (toLowerCase(word).find(x.first) != string::npos)
      if (x.first.length() > number.first.length())
      {
        number.first.assign(x.first);
        number.second = x.second;
      }      

  return number.second;
}

string TextNumbersEnglish::toLowerCase(string word) {
  std::locale loc;
  string lower;
  for (std::string::size_type i = 0; i < word.length(); ++i)
    lower += std::tolower(word[i], loc);

  return lower;
}

string TextNumbersEnglish::truncateBegining(string word) {
  string end_truncated = word;
  size_t truncate_pos = word.find_first_of("-");
  if (truncate_pos != string::npos)
    end_truncated = word.substr(0, truncate_pos);

  return end_truncated;
}

string TextNumbersEnglish::truncateEnding(string word) {
  string end_truncated = word;
  size_t truncate_pos = word.find_last_of("-");
  if (truncate_pos != string::npos)
    end_truncated = word.substr(truncate_pos, word.length());

  return end_truncated;
}


void TextNumbersEnglish::removeSpecialCharacters(string* word) {
  word->erase(std::remove_if(word->begin(), word->end(),
    [](char ch) {return !(iswalnum(ch) || ch == '_'); }),
    word->end());
}